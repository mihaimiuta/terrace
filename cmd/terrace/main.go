package main

import (
	"log"
	"mihaimiuta/terrace/internal/ui"
	"os"

	tea "github.com/charmbracelet/bubbletea"
)

func main() {
	logfilePath := os.Getenv("BUBBLETEA_LOG")
	if logfilePath != "" {
		if _, err := tea.LogToFile(logfilePath, "simple"); err != nil {
			log.Fatal(err)
		}
	}

	p := tea.NewProgram(ui.Model(5))
	if _, err := p.Run(); err != nil {
		log.Fatal(err)
	}
}
